#ifndef __TDATVALUE_H__
#define __TDATVALUE_H__

#include <iostream>

class TDatValue;
typedef TDatValue *PTDatValue;

class TDatValue
{
public:
	virtual TDatValue * GetCopy() = 0; // �������� �����
	~TDatValue() {}
};

#endif