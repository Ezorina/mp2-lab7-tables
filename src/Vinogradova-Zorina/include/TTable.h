#ifndef __TTABLE_H__
#define __TTABLE_H__

#include "TDataCom.h"
#include "TTabRecord.h"

class  TTable : public TDataCom
{
protected:
	int DataCount;  // ���������� ������� � �������
	int Efficiency; // ���������� ������������� ���������� ��������
public:
	TTable() : DataCount(0), Efficiency(0) {} // �����������
	virtual ~TTable() {}                      // ����������
											  // �������������� ������
	int GetDataCount() const { return DataCount; }    // �-�� �������
	int GetEfficiency() const { return Efficiency; }  // �������������
	int IsEmpty() const { return DataCount == 0; }    //�����?
	virtual bool IsFull() const = 0;                   // ���������?
													   // ������
	virtual TKey GetKey() const = 0;
	virtual PTDatValue GetValuePTR() const = 0;
	// �������� ������
	virtual PTDatValue FindRecord(TKey k) = 0;           // ����� ������
	virtual void InsRecord(TKey k, PTDatValue pVal) = 0; // ��������
	virtual void DelRecord(TKey k) = 0;                  // ������� ������
														 // ���������
	virtual void Reset() = 0;            // ���������� �� ������ ������
	virtual bool IsTabEnded() const = 0; // ������� ���������?
	virtual void GoNext() = 0;           // ������� � ��������� ������
										 // (=1 ����� ���������� ��� ��������� ������ �������)
};

#endif