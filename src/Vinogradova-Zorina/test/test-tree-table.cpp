#include <gtest\gtest.h>
#include "ttreetable.h"
#include "tbalancetree.h"

TEST(TTreeNode, can_create_TTreeNode) 
{
	EXPECT_NO_FATAL_FAILURE(TTreeNode node());
}

TEST(TTreeNode, can_get_tree_branch) 
{
	int* i = new int(1);
	i[0] = 5;
	TTreeNode left("Student 1", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 4;
	TTreeNode right("Student 2", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 10;
	TTreeNode Center("Student 3", (PTDatValue)&i[0], &left, &right);

	ASSERT_TRUE((Center.GetLeft() == &left) && (Center.GetRight() == &right));
}

TEST(TBalanceNode, can_created_TBalanceNode) 
{
	EXPECT_NO_FATAL_FAILURE(TBalanceNode TBN);
}

TEST(TTreeTable, can_create_TTreeTable) 
{
	EXPECT_NO_FATAL_FAILURE(TTreeTable table);
}

TEST(TTreeTable, can_insert_record) 
{
	int* i = new int(1);
	i[0] = 3;
	TTreeTable* table = new TTreeTable();
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Student", (PTDatValue)&i[0]));
}

TEST(TTreeTable, can_find_record) 
{
	int* i = new int(1);
	i[0] = 3;
	TTreeTable* table = new TTreeTable();
	table->InsRecord("Student 1", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 5;
	table->InsRecord("Student 2", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 2;
	table->InsRecord("Student 3", (PTDatValue)&i[0]);

	EXPECT_EQ(*(int*)table->FindRecord("Student 2"), 5);
}

TEST(TBalanceTree, can_create_TBalanceTree) 
{
	EXPECT_NO_FATAL_FAILURE(TBalanceTree tree);
}

TEST(TBalanceTree, can_add_and_find_record) 
{
	int* i = new int(1);
	i[0] = 3;
	TBalanceTree* table = new TBalanceTree();
	table->InsRecord("Student 1", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 5;
	table->InsRecord("Student 2", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 1;
	table->InsRecord("Student 3", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 2;
	table->InsRecord("Student 4", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 4;
	table->InsRecord("Student 5", (PTDatValue)&i[0]);

	EXPECT_EQ(*(int*)table->FindRecord("Student 3"), 1);
}