#include <gtest/gtest.h>
#include "tsorttable.h"

TEST(TScanTable, can_create_TScanTable) 
{
	EXPECT_NO_FATAL_FAILURE(TScanTable table(5));
}

TEST(TScanTable, can_insert_record) 
{
	TScanTable* table = new TScanTable(5);
	int i = 5;
	TTabRecord rec("Student 1", (TDatValue*)&i);
	table->InsRecord("Student 1", (TDatValue*)&i);
	EXPECT_TRUE(rec == *table->GetCurrRecord());
}

TEST(TScanTable, can_insert_two_records) 
{
	TScanTable* table = new TScanTable(5);
	int i = 5;
	table->InsRecord("Student 1", (TDatValue*)&i);
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Student 2", (TDatValue*)&i));
}

TEST(TScanTable, can_find_record) 
{
	TScanTable* table = new TScanTable(5);
	int* i = new int(1);
	i[0] = 5;
	table->InsRecord("Student 1", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 2;
	table->InsRecord("Student 2", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 3;
	table->InsRecord("Student 3", (TDatValue*)i);
	i = new int(1);
	i[0] = 4;
	table->InsRecord("Student 4", (TDatValue*)i);

	EXPECT_EQ(2, *((int*)(table->FindRecord("Student 2"))));
}

TEST(TScanTable, can_delete_record) 
{
	TScanTable* table = new TScanTable(5);
	int *i = new int(1);
	i[0] = 5;
	table->InsRecord("Student 1", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 2;
	table->InsRecord("Student 2", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 3;
	table->InsRecord("Student 3", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 4;
	table->InsRecord("Student 4", (TDatValue*)&i[0]);

	EXPECT_NO_FATAL_FAILURE(table->DelRecord("Student 2"));
	EXPECT_TRUE(nullptr == table->FindRecord("Student 2"));
}

TEST(TSortTable, can_create_table) 
{
	EXPECT_NO_FATAL_FAILURE(TSortTable table(5));
}

TEST(TSortTable, can_Ins_Record) 
{
	TSortTable* table = new TSortTable(5);
	int i = 5;
	TTabRecord rec("Student", (TDatValue*)&i);
	table->InsRecord("Student", (PTDatValue)&i);

	EXPECT_TRUE(rec == *table->GetCurrRecord());
}

TEST(TSortTable, can_assignment_table) 
{
	TSortTable* table = new TSortTable(5);
	int i = 5;
	table->InsRecord("Student", (PTDatValue)&i);
	TSortTable table2(2);

	table2 = *table;

	EXPECT_TRUE(table2.GetCurrRecord(), table->GetCurrRecord());
}

TEST(TSortTable, can_set_and_get_sort_metod) 
{
	TSortTable* table = new TSortTable(5);
	int i = 5;
	table->SetSortMethod(INSERT_SORT);

	EXPECT_TRUE(table->GetSortMethod() == INSERT_SORT);
}

TEST(TSortTable, can_used_all_sort__can_find_record) 
{
	TSortTable* table = new TSortTable(5);
	int* i = new int(1);
	i[0] = 5;
	table->SetSortMethod(INSERT_SORT);
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Student 1", (TDatValue*)&i[0]));
	i = new int(1);
	i[0] = 4;
	table->SetSortMethod(MERGE_SORT);
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Student 2", (TDatValue*)&i[0]));
	
	i = new int(1);
	i[0] = 4;
	table->SetSortMethod(INSERT_SORT);
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Student 3", (TDatValue*)&i[0]));

	EXPECT_EQ(4, *((int*)(table->FindRecord("Student 2"))));
}