#include "TBalanceTree.h"

int TBalanceTree::InsBalanceTree(PTBalanceNode &pNode, TKey k, PTDatValue pVal)
{
    int height = HeightOk;
    if (pNode == nullptr)
    {
        pNode = new TBalanceNode(k, pVal);
        height = HeightInc;
        DataCount++;
    }
    else if (k < pNode->Key)
    {
        if (InsBalanceTree((PTBalanceNode&)pRoot->pLeft, k, pVal) == HeightInc)
            height = LeftTreeBalancing(pNode);
    }
    else if (k > pNode->Key)
    {
        if (InsBalanceTree((PTBalanceNode&)pRoot->pRight, k, pVal) == HeightInc)
            height = RightTreeBalancing(pNode);
    }
    return height;
}

int TBalanceTree::LeftTreeBalancing(PTBalanceNode &pNode)
{
    int height = HeightOk;
    switch (pNode->Balance)
    {
    case BalRight:
        pNode->SetBalance(BalOk);
        break;
    case BalOk:
        pNode->SetBalance(BalLeft);
        height = HeightInc;
        break;
    case BalLeft:
        PTBalanceNode p1, p2;
        p1 = PTBalanceNode(pNode->pLeft);
        if (p1->Balance == BalLeft)
        {
            pNode->pLeft = p1->pRight;
            p1->pRight = pNode;
            pNode->SetBalance(BalOk);
            pNode = p1;
        }
        else
        {
            p2 = PTBalanceNode(p1->pRight);
            p1->pRight = p2->pLeft;
            p2->pLeft = p1;
            pNode->pLeft = p2->pRight;
            p2->pRight = pNode;
            if (p2->Balance == BalLeft)
                pNode->SetBalance(BalRight);
            else
                pNode->SetBalance(BalOk);
            if (p2->Balance == BalRight)
                p1->SetBalance(BalLeft);
            else
                p1->SetBalance(BalOk);
            pNode = p2;
        }
        pNode->SetBalance(BalOk);
    }
    return height;
}

int TBalanceTree::RightTreeBalancing(PTBalanceNode &pNode)
{
    int height = HeightOk;
    switch (pNode->Balance)
    {
    case BalLeft:
        pNode->SetBalance(BalOk);
        break;
    case BalOk:
        pNode->SetBalance(BalRight);
        height = HeightInc;
        break;
    case BalRight:
        PTBalanceNode p1, p2;
        p1 = PTBalanceNode(pNode->pRight);
        if (p1->Balance == BalRight)
        {
            pNode->pRight = p1->pLeft;
            p1->pLeft = pNode;
            pNode->SetBalance(BalOk);
            pNode = p1;
        }
        else
        {
            p2 = PTBalanceNode(p1->pLeft);
            p1->pLeft = p2->pRight;
            p2->pRight = p1;
            pNode->pRight = p2->pLeft;
            p2->pLeft = pNode;
            if (p2->Balance == BalRight)
                pNode->SetBalance(BalLeft);
            else
                pNode->SetBalance(BalOk);
            if (p2->Balance == BalLeft)
                p1->SetBalance(BalRight);
            else
                p1->SetBalance(BalOk);
            pNode = p2;
        }
        pNode->SetBalance(BalOk);
    }
    return height;
}

void TBalanceTree::InsRecord(TKey k, PTDatValue pVal)
{
    InsBalanceTree((PTBalanceNode&)pRoot, k, pVal);
}

void TBalanceTree::DelRecord(TKey k)
{
    if (FindRecord(k) != nullptr)
    {
        PTTreeNode tmp = pRoot, up;

        while (!St.empty())
            St.pop();
        while (tmp->Key != k)
        {
            St.push(tmp);
            if (tmp->Key > k)
                tmp = tmp->pLeft;
            else
                tmp = tmp->pRight;
        }

        TKey tmpk = tmp->Key;
        if (tmp->pLeft != nullptr && tmp->pRight != nullptr)
        {
            PTTreeNode down_left = tmp->pRight;
            while (down_left->pLeft != nullptr)
                down_left = down_left->pLeft;
            down_left->pLeft = tmp->pLeft;

            if (!St.empty())
            {
                up = St.top();
                if (up != nullptr)
                {
                    if (up->pLeft == tmp)
                        up->pLeft = nullptr;
                    else if (up->pRight == tmp)
                        up->pRight = nullptr;
                }
            }
            else
                pRoot = tmp->pRight;
        }
        else
        {
            if (!St.empty())
            {
                up = St.top();
                if (up != nullptr)
                {
                    if (up->pLeft == tmp)
                        up->pLeft = nullptr;
                    else if (up->pRight == tmp)
                        up->pRight = nullptr;
                }
            }
            else
            {
                if (tmp->pLeft == nullptr && tmp->pRight == nullptr)
                    pRoot = nullptr;
                else if (tmp->pLeft != nullptr && tmp->pRight == nullptr)
                    pRoot = tmp->pLeft;
                else if (tmp->pLeft == nullptr && tmp->pRight != nullptr)
                    pRoot = tmp->pRight;
            }
        }
        delete tmp;
        DataCount--;
        if (pRoot != nullptr)
        {
            if (tmpk < pRoot->Key)
                LeftTreeBalancing((PTBalanceNode&)pRoot);
            else if (tmpk > pRoot->Key)
                RightTreeBalancing((PTBalanceNode&)pRoot);
        }
    }
}
