### Лабораторная работа №7. Таблицы
#### Введение
Представление данных во многих задачах из разных областей человеческой деятельности может быть организовано при помощи таблиц. Таблицы представляют собой последовательности строк (записей), структура строк может быть различной, но обязательным является поле, задающее имя (ключ) записи. Таблицы применяются в бухгалтерском учете (ведомости заработной платы), в торговле (прайс-листы), в образовательных учреждениях (экзаменационные ведомости) и являются одними из наиболее распространенных структур данных, используемых при создании системного и прикладного математического обеспечения. Таблицы широко применяются в трансляторах (таблицы идентификаторов) и операционных системах, могут рассматриваться как программная реализация ассоциативной памяти и т.п. Существование отношения «иметь имя» является обязательным в большинстве разрабатываемых программистами структур данных; доступ по имени в этих структурах служит для получения соответствия между адресным принципом указания элементов памяти ЭВМ и общепринятым (более удобным для человека) способом указания объектов по их именам.

Целью лабораторной работы помимо изучения способов организации таблиц является начальное знакомство с принципами проектирования структур хранения, используемых в методах решения прикладных задач. На примере таблиц изучаются возможность выбора разных вариантов структур хранения, анализ их эффективности и определения областей приложений, в которых выбираемые структуры хранения являются наиболее эффективными.

В качестве практической задачи, на примере которой будут продемонстрированы возможные способы организации таблиц, рассматривается проблема статистической обработки результатов экзаменационной успеваемости студентов (выполнение таких, например, вычислений как определение среднего балла по предмету и/или по группе при назначении студентов на стипендию или при распределении студентов по кафедрам и т.п.).

#### Основные понятия и определения
__Таблица__ (от лат. tabula – доска) – динамическая структура данных, базисным множеством которой является семейство линейных структур из записей (базисное отношение включения определяется операциями вставки и удаления записей).

__Запись__ – кортеж, каждый элемент которого обычно именуется полем.

__Имя записи__ (ключ) – одно из полей записи, по которому обычно осуществляется поиск записей в таблице; остальные поля образуют тело записи.

__Двоичное дерево поиска__ – это представление данных в виде дерева, для которого выполняются условия:
- для любого узла (вершины) дерева существует не более двух потомков (двоичное дерево);
- для любого узла значения во всех узлах левого поддерева меньше значения в узле;
- для любого узла значения во всех узлах правого поддерева больше значения в узле.

__Хеш-функция__ – функция, ставящая в соответствие ключу номер записи в таблице (используется при организации таблиц с вычислимым входом).

#### Требования к лабораторной работе
В рамках данной лабораторной работы ставится задача создания программных средств, поддерживающих табличные динамические структуры данных (таблицы) и базовые операции над ними:
- поиск записи;
- вставка записи (без дублирования);
- удаление записи.

Выполнение операций над таблицами может осуществляться с различной степенью эффективности в зависимости от способа организации таблицы. 

В рамках лабораторной работы как показатель эффективности предлагается использовать количество операций, необходимых для выполнения операции поиска записи в таблице.
Величина этого показателя должна определяться как аналитически (при использовании тех или иных упрощающих предположений), так и экспериментально на основе проведения вычислительных экспериментов.

В лабораторной работе предлагается реализовать следующие типы таблиц:
- просмотровые (неупорядоченные);
- упорядоченные (сортированные);
- таблицы со структурами хранения на основе деревьев поиска;
- хеш-таблицы или перемешанные (с вычисляемыми адресами).

Необходимо разработать интерфейс доступа к операциям поиска, вставки и удаления, не зависящий от способа организации таблицы.

#### Структура проекта

**TTabRecord.h** – модуль с классом объектов-значений для записей таблицы.
```
class TTabRecord;
typedef string TKey;     // тип ключа записи
typedef TTabRecord *PTTabRecord;

// Класс объектов-значений для записей таблицы
class TTabRecord : public TDatValue
{
protected:
	TKey Key;           // ключ записи
	PTDatValue pValue;  // указатель на значение
public:
	TTabRecord(TKey k = "", PTDatValue pVal = nullptr) : Key(k), pValue(pVal) {}
	void SetKey(TKey k) { Key = k; }
	TKey GetKey() { return Key; }
	void SetValuePtr(PTDatValue p) { pValue = p; }
	PTDatValue GetValuePTR() { return pValue; }
	virtual TDatValue * GetCopy()
	{
		TDatValue *temp = new TTabRecord(Key, pValue);
		return temp;
	}
	TTabRecord & operator = (TTabRecord &tr)
	{
		Key = tr.Key;
		pValue = tr.pValue;
		return *this;
	}

	virtual int operator == (const TTabRecord &tr) { return Key == tr.Key; }
	virtual int operator < (const TTabRecord &tr) { return Key < tr.Key; }
	virtual int operator > (const TTabRecord &tr) { return Key > tr.Key; }
	//дружественные классы для различных типов таблиц, см. далее
	friend class TArrayTable;
	friend class TScanTable;
	friend class TSortTable;
	friend class TTreeNode;
	friend class TTreeTable;
	friend class TArrayHash;
	friend class TListHash;
};
```

**TTable.h** – абстрактный базовый класс, содержит спецификации методов таблицы;
```
class  TTable : public TDataCom
{
protected:
	int DataCount;  // количество записей в таблице
	int Efficiency; // показатель эффективности выполнения операции
public:
	TTable() : DataCount(0), Efficiency(0) {} // конструктор
	virtual ~TTable() {}                      // деструктор
											  // информационные методы
	int GetDataCount() const { return DataCount; }    // к-во записей
	int GetEfficiency() const { return Efficiency; }  // эффективность
	int IsEmpty() const { return DataCount == 0; }    //пуста?
	virtual bool IsFull() const = 0;                   // заполнена?
													   // доступ
	virtual TKey GetKey() const = 0;
	virtual PTDatValue GetValuePTR() const = 0;
	// основные методы
	virtual PTDatValue FindRecord(TKey k) = 0;           // найти запись
	virtual void InsRecord(TKey k, PTDatValue pVal) = 0; // вставить
	virtual void DelRecord(TKey k) = 0;                  // удалить запись
														 // навигация
	virtual void Reset() = 0;            // установить на первую запись
	virtual bool IsTabEnded() const = 0; // таблица завершена?
	virtual void GoNext() = 0;           // переход к следующей записи
										 // (=1 после применения для последней записи таблицы)
};
```

**TArrayTable.h**, **TArrayTable.cpp** – абстрактный базовый класс для таблиц с непрерывной памятью.
```
#define TabMaxSize 25
enum TDataPos { FIRST_POS, CURRENT_POS, LAST_POS };

class  TArrayTable : public TTable
{
protected:
	PTTabRecord *pRecs; // память для записей таблицы
	int TabSize;        // макс. возм.количество записей в таблице
	int CurrPos;        // номер текущей записи (нумерация с 0)
public:
	TArrayTable(int Size = TabMaxSize);
	~TArrayTable();
	// информационные методы
	virtual bool IsFull() const { return DataCount >= TabSize; }
	int GetTabSize() const { return TabSize; }
	// доступ
	virtual TKey GetKey() const { return GetKey(CURRENT_POS); }
	virtual PTDatValue GetValuePTR() const { return GetValuePTR(CURRENT_POS); }
	virtual TKey GetKey(TDataPos mode) const;
	virtual PTDatValue GetValuePTR(TDataPos mode) const;
	// основные методы
	virtual PTDatValue FindRecord(TKey k) = 0;           // найти запись
	virtual void InsRecord(TKey k, PTDatValue pVal) = 0; // вставить
	virtual void DelRecord(TKey k) = 0;                  // удалить запись
														 //навигация
	virtual void Reset() { CurrPos = 0; }                            // установить на первую запись
	virtual bool IsTabEnded() const { return CurrPos >= DataCount; } // таблица завершена?
	virtual void GoNext();                                           // переход к следующей записи
																	 //(=1 после применения для последней записи таблицы)
	virtual void SetCurrentPos(int pos);                             // установить текущую запись
	PTTabRecord GetCurrRecord() { return pRecs[CurrPos]; }		 // получить текущую запись
	int GetCurrentPos() const { return CurrPos; }                    //получить номер текущей записи

	friend TSortTable;
};
```

```
#include "TArrayTable.h"

TArrayTable::TArrayTable(int Size)
{
    TabSize = Size;
    CurrPos = 0;
    pRecs = new PTTabRecord[Size];
    for (int i = 0; i < Size; ++i)
        pRecs[i] = nullptr;
}

TArrayTable::~TArrayTable()
{
    for (int i = 0; i < DataCount; ++i)
        delete pRecs[i];
    delete[] pRecs;
}

TKey TArrayTable::GetKey(TDataPos mode) const
{
    int pos = -1;
    if (!IsEmpty())
    {
        switch (mode)
        {
        case FIRST_POS:
            pos = 0;
            break;
        case CURRENT_POS:
            pos = CurrPos;
            break;
        case LAST_POS:
            pos = DataCount - 1;
            break;
        }
    }
    return (pos == -1) ? "" : pRecs[pos]->Key;
}

PTDatValue TArrayTable::GetValuePTR(TDataPos mode) const
{
    int pos = -1;
    if (!IsEmpty())
    {
        switch (mode)
        {
        case FIRST_POS:
            pos = 0;
            break;
        case CURRENT_POS:
            pos = CurrPos;
            break;
        case LAST_POS:
            pos = DataCount - 1;
            break;
        }
    }
    return (pos == -1) ? nullptr : pRecs[pos]->pValue;
}

void TArrayTable::GoNext()
{
    if (!IsTabEnded())
        CurrPos++;
}

void TArrayTable::SetCurrentPos(int pos)
{
    if (pos >= 0 && pos < DataCount)
        CurrPos = pos;

}

```

**TScanTable.h**, **TScanTable.cpp** – модуль с классом, обеспечивающим реализацию просматриваемых таблиц.
```
class  TScanTable : public TArrayTable 
{
public:
	TScanTable(int Size = TabMaxSize) : TArrayTable(Size) {};//конструктор
															 // основные методы
	virtual PTDatValue FindRecord(TKey k);//найти запись
	virtual void InsRecord(TKey k, PTDatValue pVal);//вставить
	virtual void DelRecord(TKey k);//удалить запись

};

```

```
#include "TScanTable.h"

PTDatValue TScanTable::FindRecord(TKey k)
{
    PTDatValue res = nullptr;
    for (int i = 0; i < DataCount; i++)
    {
        if (pRecs[i]->Key == k)
        {
            CurrPos = i;
            res = pRecs[i]->pValue;
            break;
        }
    }
    return res;
}

void TScanTable::InsRecord(TKey k, PTDatValue pVal)
{
    for (int i = 0; i < TabSize; i++)
    {
        if (pRecs[i] == nullptr)
        {
            pRecs[i] = new TTabRecord(k, pVal);
            DataCount++;
            break;
        }
    }
}

void TScanTable::DelRecord(TKey k)
{
    for (int i = 0; i < TabSize; i++)
    {
        if (pRecs[i] != nullptr && pRecs[i]->Key == k)
        {
            delete pRecs[i];
            pRecs[i] = pRecs[DataCount - 1];
            pRecs[DataCount - 1] = nullptr;
            DataCount--;
        }
    }
}
```

**TSortTable.h**, **TSortTable.cpp** – модуль с классом, обеспечивающим реализацию упорядоченных таблиц.
```
enum TSortMethod { INSERT_SORT, MERGE_SORT, QUIQ_SORT };

class  TSortTable : public TScanTable
{
protected:
	TSortMethod SortMethod;                            // метод сортировки
	void SortData();                                   // сортировка данных
	void InsertSort(PTTabRecord *pMem, int DataCount); // метод вставок
	void MergeSort(PTTabRecord *pMem, int DataCount);  // метод слияния
	void MergeSorter(PTTabRecord * &pData, PTTabRecord * &pBuff, int Size);
	void MergeData(PTTabRecord *&pData, PTTabRecord *&pBuff, int n1, int n2);
	void QuiqSort(PTTabRecord *pMem, int DataCount);   // быстрая сортировка
	void QuiqSplit(PTTabRecord *pData, int Size, int &Pivot);
public:
	TSortTable(int Size = TabMaxSize) : TScanTable(Size) {} // конструктор
	TSortTable(const TScanTable &tab);                      // из просматриваемой таблицы
	TSortTable & operator=(const TScanTable &tab);          // присваивание
	TSortMethod GetSortMethod() { return SortMethod; }      // получить метод сортировки
	void SetSortMethod(TSortMethod sm) { SortMethod = sm; } // установить метод сортировки
															// основные методы
	virtual PTDatValue FindRecord(TKey k);           // найти запись
	virtual void InsRecord(TKey k, PTDatValue pVal); // вставить
	virtual void DelRecord(TKey k);                  // удалить запись
};
```

```
#include "TSortTable.h"

void TSortTable::SortData()
{
    switch (SortMethod)
    {
    case INSERT_SORT:
        InsertSort(pRecs, DataCount);
        break;
    case MERGE_SORT:
        MergeSort(pRecs, DataCount);
        break;
    case QUIQ_SORT:
        QuiqSort(pRecs, DataCount);
        break;
    }
}

void TSortTable::InsertSort(PTTabRecord *pMem, int DataCount)
{
    PTTabRecord pR;
    for (int i = 1; i < DataCount; i++)
    {
        pR = pRecs[i];
        int j = i - 1;
        while (j >= 0 && pRecs[j]->Key > pR->Key)
        {
            pRecs[j + 1] = pRecs[j];
            j--;
        }
        pRecs[j + 1] = pR;
    }
}

void TSortTable::MergeSort(PTTabRecord *pMem, int DataCount)
{
    PTTabRecord *pData = pRecs;
    PTTabRecord *pBuff = new PTTabRecord[DataCount];
    PTTabRecord *pTemp = pBuff;
    MergeSorter(pData, pBuff, DataCount);
    if (pData == pTemp)
    {
        for (int i = 0; i < DataCount; i++)
            pBuff[i] = pData[i];
    }
    delete pTemp;
}

void TSortTable::MergeSorter(PTTabRecord * &pData, PTTabRecord * &pBuff, int Size)
{
    int n1 = (Size + 1) / 2;
    int n2 = Size - n1;
    if (Size > 2)
    {
        PTTabRecord *pDat2 = pData + n1, *pBuff2 = pBuff + n1;
        MergeSorter(pData, pBuff, n1);
        MergeSorter(pDat2, pBuff2, n2);
    }
    MergeData(pData, pBuff, n1, n2);
}

void TSortTable::MergeData(PTTabRecord *&pData, PTTabRecord *&pBuff, int n1, int n2)
{
    for (int i = 0; i < (n1 + n2); i++)
        pBuff[i] = pData[i];
    PTTabRecord *&tmp = pData;
    pData = pBuff;
    pBuff = tmp;
}

void TSortTable::QuiqSort(PTTabRecord *pMem, int DataCount)
{
    int pivot = 0;
    int n1, n2;
    if (DataCount > 1) {
        QuiqSplit(pRecs, DataCount, pivot);
        n1 = pivot + 1;
        n2 = DataCount - n1;
        QuiqSort(pRecs, n1 - 1);
        QuiqSort(pRecs + n1, n2);
    }
}

void TSortTable::QuiqSplit(PTTabRecord *pData, int Size, int &Pivot)
{
    PTTabRecord pPivot = pData[0], pTemp;
    int i1 = 1, i2 = Size - 1;
    while (i1 <= i2) {
        while ((i1 < Size) && !(pData[i1]->GetKey() > pPivot->GetKey())) i1++;
        while (pData[i2]->GetKey() > pPivot->GetKey()) i2--;
        if (i1 < i2) {
            pTemp = pData[i1];
            pData[i1] = pData[i2];
            pData[i2] = pTemp;
        }
    }
    pData[0] = pData[i2];
    pData[i2] = pPivot;
    Pivot = i2;
}

TSortTable::TSortTable(const TScanTable &tab)
{
    *this = tab;
}

TSortTable & TSortTable::operator=(const TScanTable &tab)
{
    if (pRecs != nullptr)
    {
        for (int i = 0; i < DataCount; i++)
            delete pRecs[i];
        delete[] pRecs;
        pRecs = nullptr;
    }
    TabSize = tab.TabSize;
    DataCount = tab.DataCount;
    pRecs = new PTTabRecord[TabSize];
    for (int i = 0; i < DataCount; i++)
        pRecs[i] = (PTTabRecord)tab.pRecs[i]->GetCopy();
    SortData();
    CurrPos = 0;
    return *this;
}

// основные методы

PTDatValue TSortTable::FindRecord(TKey k)
{
    PTDatValue res = nullptr;
    if (DataCount != 0)
    {
        int i, iup = 0, idown = DataCount - 1;
        while (iup <= idown)
        {
            i = (iup + idown) / 2;
            if (pRecs[i]->Key == k)
            {
                res = pRecs[i]->pValue;
                break;
            }
            else if (pRecs[i]->Key > k)
                idown = i - 1;
            else
                iup = i + 1;
        }
    }
    return res;
}

void TSortTable::InsRecord(TKey k, PTDatValue pVal)
{
    if (!IsFull())
	{
		if (FindRecord(k) == nullptr)  
		{
			for (int i = DataCount; i > CurrPos; i--)
				pRecs[i] = pRecs[i - 1];
			pRecs[CurrPos] = new TTabRecord(k, pVal);
			DataCount++;
			SortData();
		}
	}

}

void TSortTable::DelRecord(TKey k)
{
    if (!IsEmpty())
    {
        for (int i = 0; i < DataCount; i++)
        {
            if (pRecs[i]->Key == k)
                delete pRecs[i];
            for (int j = i; j < DataCount; j++)
                pRecs[j] = pRecs[j + 1];
            DataCount--;
        }
    }
}
```

**TTreeNode.h**, **TTreeNode.cpp** – модуль с абстрактным базовым классом объектов-значений для деревьев.
```
typedef  TTreeNode *PTTreeNode;

class  TTreeNode : public TTabRecord
{
protected:
	PTTreeNode pLeft, pRight; // указатели на поддеревья
public:
	TTreeNode(TKey k = "", PTDatValue pVal = nullptr, PTTreeNode pL = nullptr,
		PTTreeNode pR = nullptr) : TTabRecord(k, pVal), pLeft(pL), pRight(pR) {}
	PTTreeNode GetLeft() const { return pLeft; }
	PTTreeNode GetRight() const { return pRight; }
	virtual TDatValue * GetCopy();
	friend class TTreeTable;
	friend class TBalanceTree;
};
```

```
TDatValue * TTreeNode::GetCopy()
{
    //return new TTreeNode(Key, pValue, nullptr, nullptr);
    TTreeNode *tmp = new TTreeNode(Key, pValue, nullptr, nullptr);
    return tmp;
}

```

**TTreeTable.h**, **TTreeTable.cpp** – модуль с классом, реализующим таблицы в виде деревьев поиска.
```
class  TTreeTable : public TTable
{
protected:
	PTTreeNode pRoot;       // указатель на корень дерева
	PTTreeNode *ppRef;      // адрес указателя на вершину-результата в FindRecord
	PTTreeNode pCurrent;    // указатель на текущую вершину
	int CurrPos;            // номер текущей вершины
	stack < PTTreeNode> St; // стек для итератора
	void DeleteTreeTab(PTTreeNode pNode); // удаление
public:
	TTreeTable() : TTable(), CurrPos(0), ppRef(nullptr) { pRoot = pCurrent = nullptr; }
	~TTreeTable() { DeleteTreeTab(pRoot); } // деструктор
											// информационные методы
	virtual bool IsFull() const { return false; } //заполнена?
												  //основные методы
	virtual PTDatValue FindRecord(TKey k);           // найти запись
	virtual void InsRecord(TKey k, PTDatValue pVal); // вставить
	virtual void DelRecord(TKey k);                  // удалить запись
													 // навигация
	virtual TKey GetKey() const;
	virtual PTDatValue GetValuePTR() const;
	virtual void Reset();            // установить на первую запись
	virtual bool IsTabEnded() const; // таблица завершена?
	virtual void GoNext();           // переход к следующей записи
									 //(=1 после применения для последней записи таблицы)
};
```

```
void TTreeTable::DeleteTreeTab(PTTreeNode pNode)
{
    if (pNode != nullptr)
    {
        DeleteTreeTab(pNode->pLeft);
        DeleteTreeTab(pNode->pRight);
        delete pNode;
    }
}

PTDatValue TTreeTable::FindRecord(TKey k)
{
    PTTreeNode pNode = pRoot;
    ppRef = &pRoot;
    while (pNode != nullptr)
    {
        if (pNode->Key == k)
            break;
        else if (pNode->Key > k)
            ppRef = &pNode->pLeft;
        else
            ppRef = &pNode->pRight;
        pNode = *ppRef;
    }
    return (pNode == nullptr) ? nullptr : pNode->pValue;
}

void TTreeTable::InsRecord(TKey k, PTDatValue pVal)
{
    if (FindRecord(k) == nullptr)
    {
        *ppRef = new TTreeNode(k, pVal);
        DataCount++;
    }
}

void TTreeTable::DelRecord(TKey k)
{
    if (FindRecord(k) != nullptr)
    {
        PTTreeNode tmp = pRoot, up;

        while (!St.empty())
            St.pop();
        while (tmp->Key != k)
        {
            St.push(tmp);
            if (tmp->Key > k)
                tmp = tmp->pLeft;
            else
                tmp = tmp->pRight;
        }

        if (tmp->pLeft != nullptr && tmp->pRight != nullptr)
        {
            PTTreeNode down_left = tmp->pRight;
            while (down_left->pLeft != nullptr)
                down_left = down_left->pLeft;
            down_left->pLeft = tmp->pLeft;

            if (!St.empty())
            {
                up = St.top();
                if (up != nullptr)
                {
                    if (up->pLeft == tmp)
                        up->pLeft = nullptr;
                    else if (up->pRight == tmp)
                        up->pRight = nullptr;
                }
            }
            else
                pRoot = tmp->pRight;
        }
        else
        {
            if (!St.empty())
            {
                up = St.top();
                if (up != nullptr)
                {
                    if (up->pLeft == tmp)
                        up->pLeft = nullptr;
                    else if (up->pRight == tmp)
                        up->pRight = nullptr;
                }
            }
            else
            {
                if (tmp->pLeft == nullptr && tmp->pRight == nullptr)
                    pRoot = nullptr;
                else if (tmp->pLeft != nullptr && tmp->pRight == nullptr)
                    pRoot = tmp->pLeft;
                else if (tmp->pLeft == nullptr && tmp->pRight != nullptr)
                    pRoot = tmp->pRight;
            }
        }
        delete tmp;
        DataCount--;
    }
}

TKey TTreeTable::GetKey() const
{
    return (pCurrent == nullptr) ? "" : pCurrent->Key;
}

PTDatValue TTreeTable::GetValuePTR() const
{
    return (pCurrent == nullptr) ? nullptr : pCurrent->pValue;
}

void TTreeTable::Reset()
{
    PTTreeNode pNode = pCurrent = pRoot;
    while (!St.empty())
        St.pop();
    CurrPos = 0;
    while (pNode != nullptr)
    {
        St.push(pNode);
        pCurrent = pNode;
        pNode = pNode->pLeft;
    }
}

bool TTreeTable::IsTabEnded() const
{
    return CurrPos >= DataCount;
}

void TTreeTable::GoNext()
{
    CurrPos++;
    if (!IsTabEnded() && (pCurrent != nullptr))
    {
        PTTreeNode pNode = pCurrent = pCurrent->GetRight();
        St.pop();
        while (pNode != nullptr)
        {
            St.push(pNode);
            pCurrent = pNode;
            pNode = pNode->GetLeft();
        }
        if ((pCurrent == nullptr) && !St.empty())
            pCurrent = St.top();
    }
}
```

**TBalanceNode.h**, **TBalanceNode.cpp** – модуль с базовым классом объектов-значений для сбалансированных деревьев.
```
#define BalOk 0
#define BalLeft -1
#define BalRight 1
class  TBalanceNode;
typedef TBalanceNode *PTBalanceNode;

class  TBalanceNode : public TTreeNode
{
protected:
	int Balance; // индекс балансировки вершины (-1,0,1)
public:
	TBalanceNode(TKey k = "", PTDatValue pVal = nullptr, PTTreeNode pL = nullptr,
		PTTreeNode pR = nullptr, int bal = BalOk) : TTreeNode(k, pVal, pL, pR),
		Balance(bal) {} // конструктор
	virtual TDatValue * GetCopy();  // изготовить копию
	int GetBalance() const { return Balance; }
	void SetBalance(int bal) { Balance = bal; }
	friend class TBalanceTree;
};
```

```
#include "TBalanceNode.h"

TDatValue * TBalanceNode::GetCopy()
{
    TBalanceNode *tmp = new TBalanceNode(Key, pValue, nullptr, nullptr, BalOk);
    return tmp;
}
```

**TBalanceTree.h**, **TBalanceTree.cpp** – модуль с классом, реализующим таблицы в виде сбалансированных деревьев поиска.
```
#define HeightOk 0
#define HeightInc 1

class  TBalanceTree : public TTreeTable
{
protected:
	int InsBalanceTree(PTBalanceNode &pNode, TKey k, PTDatValue pVal);
	int LeftTreeBalancing(PTBalanceNode &pNode);   // баланс. левого поддерева
	int RightTreeBalancing(PTBalanceNode &pNode);  // баланс. правого поддерева
public:
	TBalanceTree() : TTreeTable() {} // конструктор
									 //основные методы
	virtual void InsRecord(TKey k, PTDatValue pVal);  // вставить
	virtual void DelRecord(TKey k);                   // удалить
};
```

```
#include "TBalanceTree.h"

int TBalanceTree::InsBalanceTree(PTBalanceNode &pNode, TKey k, PTDatValue pVal)
{
    int height = HeightOk;
    if (pNode == nullptr)
    {
        pNode = new TBalanceNode(k, pVal);
        height = HeightInc;
        DataCount++;
    }
    else if (k < pNode->Key)
    {
        if (InsBalanceTree((PTBalanceNode&)pRoot->pLeft, k, pVal) == HeightInc)
            height = LeftTreeBalancing(pNode);
    }
    else if (k > pNode->Key)
    {
        if (InsBalanceTree((PTBalanceNode&)pRoot->pRight, k, pVal) == HeightInc)
            height = RightTreeBalancing(pNode);
    }
    return height;
}

int TBalanceTree::LeftTreeBalancing(PTBalanceNode &pNode)
{
    int height = HeightOk;
    switch (pNode->Balance)
    {
    case BalRight:
        pNode->SetBalance(BalOk);
        break;
    case BalOk:
        pNode->SetBalance(BalLeft);
        height = HeightInc;
        break;
    case BalLeft:
        PTBalanceNode p1, p2;
        p1 = PTBalanceNode(pNode->pLeft);
        if (p1->Balance == BalLeft)
        {
            pNode->pLeft = p1->pRight;
            p1->pRight = pNode;
            pNode->SetBalance(BalOk);
            pNode = p1;
        }
        else
        {
            p2 = PTBalanceNode(p1->pRight);
            p1->pRight = p2->pLeft;
            p2->pLeft = p1;
            pNode->pLeft = p2->pRight;
            p2->pRight = pNode;
            if (p2->Balance == BalLeft)
                pNode->SetBalance(BalRight);
            else
                pNode->SetBalance(BalOk);
            if (p2->Balance == BalRight)
                p1->SetBalance(BalLeft);
            else
                p1->SetBalance(BalOk);
            pNode = p2;
        }
        pNode->SetBalance(BalOk);
    }
    return height;
}

int TBalanceTree::RightTreeBalancing(PTBalanceNode &pNode)
{
    int height = HeightOk;
    switch (pNode->Balance)
    {
    case BalLeft:
        pNode->SetBalance(BalOk);
        break;
    case BalOk:
        pNode->SetBalance(BalRight);
        height = HeightInc;
        break;
    case BalRight:
        PTBalanceNode p1, p2;
        p1 = PTBalanceNode(pNode->pRight);
        if (p1->Balance == BalRight)
        {
            pNode->pRight = p1->pLeft;
            p1->pLeft = pNode;
            pNode->SetBalance(BalOk);
            pNode = p1;
        }
        else
        {
            p2 = PTBalanceNode(p1->pLeft);
            p1->pLeft = p2->pRight;
            p2->pRight = p1;
            pNode->pRight = p2->pLeft;
            p2->pLeft = pNode;
            if (p2->Balance == BalRight)
                pNode->SetBalance(BalLeft);
            else
                pNode->SetBalance(BalOk);
            if (p2->Balance == BalLeft)
                p1->SetBalance(BalRight);
            else
                p1->SetBalance(BalOk);
            pNode = p2;
        }
        pNode->SetBalance(BalOk);
    }
    return height;
}

void TBalanceTree::InsRecord(TKey k, PTDatValue pVal)
{
    InsBalanceTree((PTBalanceNode&)pRoot, k, pVal);
}

void TBalanceTree::DelRecord(TKey k)
{
    if (FindRecord(k) != nullptr)
    {
        PTTreeNode tmp = pRoot, up;

        while (!St.empty())
            St.pop();
        while (tmp->Key != k)
        {
            St.push(tmp);
            if (tmp->Key > k)
                tmp = tmp->pLeft;
            else
                tmp = tmp->pRight;
        }

        TKey tmpk = tmp->Key;
        if (tmp->pLeft != nullptr && tmp->pRight != nullptr)
        {
            PTTreeNode down_left = tmp->pRight;
            while (down_left->pLeft != nullptr)
                down_left = down_left->pLeft;
            down_left->pLeft = tmp->pLeft;

            if (!St.empty())
            {
                up = St.top();
                if (up != nullptr)
                {
                    if (up->pLeft == tmp)
                        up->pLeft = nullptr;
                    else if (up->pRight == tmp)
                        up->pRight = nullptr;
                }
            }
            else
                pRoot = tmp->pRight;
        }
        else
        {
            if (!St.empty())
            {
                up = St.top();
                if (up != nullptr)
                {
                    if (up->pLeft == tmp)
                        up->pLeft = nullptr;
                    else if (up->pRight == tmp)
                        up->pRight = nullptr;
                }
            }
            else
            {
                if (tmp->pLeft == nullptr && tmp->pRight == nullptr)
                    pRoot = nullptr;
                else if (tmp->pLeft != nullptr && tmp->pRight == nullptr)
                    pRoot = tmp->pLeft;
                else if (tmp->pLeft == nullptr && tmp->pRight != nullptr)
                    pRoot = tmp->pRight;
            }
        }
        delete tmp;
        DataCount--;
        if (pRoot != nullptr)
        {
            if (tmpk < pRoot->Key)
                LeftTreeBalancing((PTBalanceNode&)pRoot);
            else if (tmpk > pRoot->Key)
                RightTreeBalancing((PTBalanceNode&)pRoot);
        }
    }
}
```

**test-array-table.cpp**, **test-tree-table** – модули программы тестирования.

```
#include <gtest/gtest.h>
#include "tsorttable.h"

TEST(TScanTable, can_create_TScanTable) 
{
	EXPECT_NO_FATAL_FAILURE(TScanTable table(5));
}

TEST(TScanTable, can_insert_record) 
{
	TScanTable* table = new TScanTable(5);
	int i = 5;
	TTabRecord rec("Student 1", (TDatValue*)&i);
	table->InsRecord("Student 1", (TDatValue*)&i);
	EXPECT_TRUE(rec == *table->GetCurrRecord());
}

TEST(TScanTable, can_insert_two_records) 
{
	TScanTable* table = new TScanTable(5);
	int i = 5;
	table->InsRecord("Student 1", (TDatValue*)&i);
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Student 2", (TDatValue*)&i));
}

TEST(TScanTable, can_find_record) 
{
	TScanTable* table = new TScanTable(5);
	int* i = new int(1);
	i[0] = 5;
	table->InsRecord("Student 1", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 2;
	table->InsRecord("Student 2", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 3;
	table->InsRecord("Student 3", (TDatValue*)i);
	i = new int(1);
	i[0] = 4;
	table->InsRecord("Student 4", (TDatValue*)i);

	EXPECT_EQ(2, *((int*)(table->FindRecord("Student 2"))));
}

TEST(TScanTable, can_delete_record) 
{
	TScanTable* table = new TScanTable(5);
	int *i = new int(1);
	i[0] = 5;
	table->InsRecord("Student 1", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 2;
	table->InsRecord("Student 2", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 3;
	table->InsRecord("Student 3", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 4;
	table->InsRecord("Student 4", (TDatValue*)&i[0]);

	EXPECT_NO_FATAL_FAILURE(table->DelRecord("Student 2"));
	EXPECT_TRUE(nullptr == table->FindRecord("Student 2"));
}

TEST(TSortTable, can_create_table) 
{
	EXPECT_NO_FATAL_FAILURE(TSortTable table(5));
}

TEST(TSortTable, can_Ins_Record) 
{
	TSortTable* table = new TSortTable(5);
	int i = 5;
	TTabRecord rec("Student", (TDatValue*)&i);
	table->InsRecord("Student", (PTDatValue)&i);

	EXPECT_TRUE(rec == *table->GetCurrRecord());
}

TEST(TSortTable, can_assignment_table) 
{
	TSortTable* table = new TSortTable(5);
	int i = 5;
	table->InsRecord("Student", (PTDatValue)&i);
	TSortTable table2(2);

	table2 = *table;

	EXPECT_TRUE(table2.GetCurrRecord(), table->GetCurrRecord());
}

TEST(TSortTable, can_set_and_get_sort_metod) 
{
	TSortTable* table = new TSortTable(5);
	int i = 5;
	table->SetSortMethod(INSERT_SORT);

	EXPECT_TRUE(table->GetSortMethod() == INSERT_SORT);
}

TEST(TSortTable, can_used_all_sort__can_find_record) 
{
	TSortTable* table = new TSortTable(5);
	int* i = new int(1);
	i[0] = 5;
	table->SetSortMethod(INSERT_SORT);
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Student 1", (TDatValue*)&i[0]));
	i = new int(1);
	i[0] = 4;
	table->SetSortMethod(MERGE_SORT);
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Student 2", (TDatValue*)&i[0]));
	
	i = new int(1);
	i[0] = 4;
	table->SetSortMethod(INSERT_SORT);
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Student 3", (TDatValue*)&i[0]));

	EXPECT_EQ(4, *((int*)(table->FindRecord("Student 2"))));
}
```

```
#include <gtest\gtest.h>
#include "ttreetable.h"
#include "tbalancetree.h"

TEST(TTreeNode, can_create_TTreeNode) 
{
	EXPECT_NO_FATAL_FAILURE(TTreeNode node());
}

TEST(TTreeNode, can_get_tree_branch) 
{
	int* i = new int(1);
	i[0] = 5;
	TTreeNode left("Student 1", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 4;
	TTreeNode right("Student 2", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 10;
	TTreeNode Center("Student 3", (PTDatValue)&i[0], &left, &right);

	ASSERT_TRUE((Center.GetLeft() == &left) && (Center.GetRight() == &right));
}

TEST(TBalanceNode, can_created_TBalanceNode) 
{
	EXPECT_NO_FATAL_FAILURE(TBalanceNode TBN);
}

TEST(TTreeTable, can_create_TTreeTable) 
{
	EXPECT_NO_FATAL_FAILURE(TTreeTable table);
}

TEST(TTreeTable, can_insert_record) 
{
	int* i = new int(1);
	i[0] = 3;
	TTreeTable* table = new TTreeTable();
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Student", (PTDatValue)&i[0]));
}

TEST(TTreeTable, can_find_record) 
{
	int* i = new int(1);
	i[0] = 3;
	TTreeTable* table = new TTreeTable();
	table->InsRecord("Student 1", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 5;
	table->InsRecord("Student 2", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 2;
	table->InsRecord("Student 3", (PTDatValue)&i[0]);

	EXPECT_EQ(*(int*)table->FindRecord("Student 2"), 5);
}

TEST(TBalanceTree, can_create_TBalanceTree) 
{
	EXPECT_NO_FATAL_FAILURE(TBalanceTree tree);
}

TEST(TBalanceTree, can_add_and_find_record) 
{
	int* i = new int(1);
	i[0] = 3;
	TBalanceTree* table = new TBalanceTree();
	table->InsRecord("Student 1", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 5;
	table->InsRecord("Student 2", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 1;
	table->InsRecord("Student 3", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 2;
	table->InsRecord("Student 4", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 4;
	table->InsRecord("Student 5", (PTDatValue)&i[0]);

	EXPECT_EQ(*(int*)table->FindRecord("Student 3"), 1);
}
```

##### Результаты тестирования

![radikal.ru](http://i013.radikal.ru/1705/0b/891728dbadf9.png)

#### Вывод
В ходе лабораторной работы мы научились создавать разного рода таблицы и работать с ними, включая доступ, вставку и удаление, а также поиск элемента по ключу.